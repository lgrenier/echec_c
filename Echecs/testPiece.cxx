/** 
 * Programme test de Piece
 *
 * @file testPiece.cxx
 */

// Utile pour l'affichage
#include <iostream>
#include "Piece.h"
#include "Joueur.h"
#include "Echiquier.h"

// Pour utiliser les flux de iostream sans mettre "std::" tout le temps.
using namespace std;

bool compare(Piece &a, Piece &b)
{
  return ( (a.x()==b.x()) && (a.y() == b.y()) );
}

/**
 * Programme principal
 */
int main( int argc, char** argv )
{
  //Joueur jb(true);
  //Joueur jn(false);
  JoueurBlanc jb;
  JoueurNoir jn;
  //jb.affiche();
  //jn.affiche();

  /*
  Roi rb(true);
  Roi rn(false);
  Reine qb(true);
  Reine qn(false);
  Tour tbg(true,false);
  Tour tbd(true,true);
  Fou fbg(true,false);
  Fou fbd(true,true);
  Tour tng(false,false);
  Tour tnd(false,true);
  Fou fng(false,false);
  Fou fnd(false,true);
  */
  
  Echiquier e;
  jb.placerPieces(e);
  jn.placerPieces(e);

  /*
  e.enleverPiece(1,1);
  e.enleverPiece(1,8);
  e.enleverPiece(3,1);
  e.enleverPiece(3,8);
  e.enleverPiece(4,1);
  e.enleverPiece(4,8);
  e.enleverPiece(5,1);
  e.enleverPiece(5,8);
  e.enleverPiece(6,1);
  e.enleverPiece(6,8);
  e.enleverPiece(8,1);
  e.enleverPiece(8,8);
  e.placer(&rb);
  e.placer(&rn);
  e.placer(&qb);
  e.placer(&qn);
  e.placer(&tbg);
  e.placer(&tbd);
  e.placer(&fbg);
  e.placer(&fbd);
  e.placer(&tng);
  e.placer(&tnd);
  e.placer(&fng);
  e.placer(&fnd);
  */
  //e.getPiece();
  
  //Si les rois sont pr�sents, le jeu continu
  Piece* roib = e.getPiece(5,1);
  Piece* roin = e.getPiece(5,8);

  int x;
  int y;
  string piece;

  while (roib !=0 && roin !=0){
    cout << "quelle piece ?";
    cin >> piece;
    cout << "x ?";
    cin >> x;
    cout << "y ?";
    cin >> y;
    //D�truire piece
    e.enleverPiece(x,y);
    e.affiche();
    cout << x << " - " << y << endl;
    cout << roib << " - " << roin << endl;
  }
}
