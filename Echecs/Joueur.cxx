#include <iostream>
#include "Joueur.h"
#include "Piece.h"

JoueurBlanc::JoueurBlanc()
{
  Roi *pr = new Roi(true);
  Reine *pq = new Reine(true);
  Fou *pfl = new Fou(true,true);
  Fou *pfr = new Fou(true,false);
  Tour *ptl = new Tour(true,true);
  Tour *ptr = new Tour(true,false);

  m_pieces.push_back(pr);
  m_pieces.push_back(pq);
  m_pieces.push_back(pfl);
  m_pieces.push_back(pfr);
  m_pieces.push_back(ptl);
  m_pieces.push_back(ptr);

  for (int i=1; i<=8; i++)
    {
      Piece *p = new Piece(i,2,true); // Pions
      m_pieces.push_back(p);
    }

  // Cavaliers
  Piece *pcl = new Piece(2,1,true);
  m_pieces.push_back(pcl);
  Piece *pcr = new Piece(7,1,true);
  m_pieces.push_back(pcr);
  
  //cout << "Constructeur Joueur Blanc" << endl;
}

JoueurNoir::JoueurNoir()
{
  Roi *pr = new Roi(false);
  Reine *pq = new Reine(false);
  Fou *pfl = new Fou(false,true);
  Fou *pfr = new Fou(false,false);
  Tour *ptl = new Tour(false,true);
  Tour *ptr = new Tour(false,false);

  m_pieces.push_back(pr);
  m_pieces.push_back(pq);
  m_pieces.push_back(pfl);
  m_pieces.push_back(pfr);
  m_pieces.push_back(ptl);
  m_pieces.push_back(ptr);

  for (int i=1; i<=8; i++)
    {
      Piece *p = new Piece(i,7,false); // Pions
      m_pieces.push_back(p);
    }

  // Cavaliers
  Piece *pcl = new Piece(2,8,false);
  m_pieces.push_back(pcl);
  Piece *pcr = new Piece(7,8,false);
  m_pieces.push_back(pcr);
  
  //cout << "Constructeur Joueur Noir" << endl;
}

Joueur::Joueur()
{
  //cout << "Constructeur Joueur par defaut" << endl;
}

Joueur::Joueur(bool white)
{
  //cout << "Constructeur Joueur specialise" << endl;
}

void
Joueur::AQuiLeTour(bool couleur)
{
  if(couleur == true){
    //Au joueur blanc de jouer
  }
  else{
    //Au joueur noir de jouer
  }

}
// EXPLICATIONS :
// n++ : retourner n puis n=n+1
// ++n : n=n+1 puis retourner n

void
Joueur::placerPieces(Echiquier &e)
{
  for (int i=0; i<16; i++)
    e.placer( m_pieces[i] ); 
}

void
Joueur::affiche()
{
  for (int i=0; i<16; i++)
    m_pieces[i]->affiche();
}

bool
JoueurBlanc::isWhite()
{
  return true;
}

bool
JoueurNoir::isWhite()
{
  return false;
}
